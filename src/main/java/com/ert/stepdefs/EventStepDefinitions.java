package com.ert.stepdefs;

import com.ert.config.variables.GlobalVariables;
import com.ert.libs.pages.EventPageForUser;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class EventStepDefinitions {

    private EventPageForUser eventPageForUser;
    private Logger logger = LogManager.getLogger("ApplicationLogs");
    private String sStatus;

    @Then("^The Propose A Talk button should be displayed$")
    public void verifyVisibilityOfProposeTalkButton() {
        eventPageForUser = GlobalVariables.applicationContext.getBean(EventPageForUser.class);
        sStatus = eventPageForUser.verifyProposeATalkButtonIsDisplayed();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.verifyVisibilityOfProposeTalkButton(..) step failed : Propose a talk button not displayed. "
                            + sStatus);
            Assert.fail("Propose a talk button not displayed. " + sStatus);
        }
        logger.log(Level.INFO, "StepDefinitions.verifyVisibilityOfProposeTalkButton(..) step passed");

    }

    @When("^The User Clicks the Propose a Talk button$")
    public void clickOnPropseATalk() {
        eventPageForUser = GlobalVariables.applicationContext.getBean(EventPageForUser.class);
        sStatus = eventPageForUser.clickOnProposeATalkButton();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.clickOnPropseATalk(..) step failed : Propose a talk button not clicked. "
                            + sStatus);
            Assert.fail("Propose a talk button not clicked. " + sStatus);
        }
        logger.log(Level.INFO, "StepDefinitions.clickOnPropseATalk(..) step passed");
    }

    @And("^The User Fills the form and clicks on register$")
    public void fillTheFormAndClickRegister() {
        eventPageForUser = GlobalVariables.applicationContext.getBean(EventPageForUser.class);
        sStatus = eventPageForUser.fillsTheFormForTheTalk();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.fillTheFormAndClickRegister(..) step failed : Fill the form and click Register failed. "
                            + sStatus);
            Assert.fail("Fill the form and click Register failed. " + sStatus);
        }
        logger.log(Level.INFO, "StepDefinitions.fillTheFormAndClickRegister(..) step passed");
    }

    @Then("^The User should be presented with the suitable success message$")
    public void verifySuccessMessage() {
        eventPageForUser = GlobalVariables.applicationContext.getBean(EventPageForUser.class);
        sStatus = eventPageForUser.verifySuccessMessage();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.verifySuccessMessage(..) step failed : Failed to verify success message. "
                            + sStatus);
            Assert.fail("Failed to verify success message. " + sStatus);
        }
        logger.log(Level.INFO, "StepDefinitions.verifySuccessMessage(..) step passed");
    }

    @When("^The User Opens the \"([^\"]*)\" page$")
    public void openPage(String pageName) {
        eventPageForUser = GlobalVariables.applicationContext.getBean(EventPageForUser.class);
        sStatus = eventPageForUser.clickOnSpecificPage(pageName);
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.openPage(..) step failed : Failed to open page, " + pageName + ". "
                            + sStatus);
            Assert.fail("Failed to open page, " + pageName + ". " + sStatus);
        }
        logger.log(Level.INFO, "StepDefinitions.openPage(..) step passed");
    }

    @Then("^Validates the details for \"([^\"]*)\" page$")
    public void validatePageDetails(String pageName) {
        eventPageForUser = GlobalVariables.applicationContext.getBean(EventPageForUser.class);
        sStatus = eventPageForUser.validateDetailsForPage(pageName);
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.validatePageDetails(..) step failed : Failed to verify details for page, " + pageName + ". "
                            + sStatus);
            Assert.fail("Failed to verify details for page, " + pageName + ". " + sStatus);
        }
        logger.log(Level.INFO, "StepDefinitions.validatePageDetails(..) step passed");
    }

    @And("^The User checks the order of pages in top menu$")
    public void validateSequenceOfPageTabs() {
        eventPageForUser = GlobalVariables.applicationContext.getBean(EventPageForUser.class);
        sStatus = eventPageForUser.validateSequenceOfPageTabs();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.validateSequenceOfPageTabs(..) step failed : Failed to verify order of pages in top menu. "
                            + sStatus);
            Assert.fail("Failed to verify order of pages in top menu. " + sStatus);
        }
        logger.log(Level.INFO, "StepDefinitions.validateSequenceOfPageTabs(..) step passed");
    }


}

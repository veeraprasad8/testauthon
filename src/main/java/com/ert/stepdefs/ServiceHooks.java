package com.ert.stepdefs;

import com.ert.config.driverConfigs.DriverConfiguration;
import com.ert.config.variables.GlobalVariables;
import com.ert.libs.commonUtils.Utilities;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.AfterStep;
import cucumber.api.java.Before;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServiceHooks {

    private WebDriver driver;
    private Logger logger = LogManager.getLogger("ApplicationLogs");
    private int currentStepIndex = -1;

    /**
     * Method to initialise the test by checking the driver current state and
     * instantiating it
     *
     * @param scenario
     * @throws IOException
     */
    @Before
    public void initializeTest(Scenario scenario) throws IOException {
        // Code to setup initial configurations
        String scenarioName = scenario.getName();
        //scenario.getSourceTagNames()
        System.out.println("scenarioName" + scenarioName);
        if (scenarioName.contains("Web")) {
            driver = GlobalVariables.applicationContext.getBean("chrome", WebDriver.class);
            System.out.println("Web" + driver.toString());
            if (driver.toString().contains("(null)")) {
                GlobalVariables.applicationContext = new AnnotationConfigApplicationContext(DriverConfiguration.class);
                driver = GlobalVariables.applicationContext.getBean("chrome", WebDriver.class);
            }
        } else if (scenarioName.contains("Mobile")) {
            System.out.println("mobile:::");
            driver = GlobalVariables.applicationContext.getBean("mobile", WebDriver.class);
            //System.out.println("Mobile"+driver.toString());
            // if (driver.toString().contains("(null)")) {
            GlobalVariables.applicationContext = new AnnotationConfigApplicationContext(DriverConfiguration.class);
            driver = GlobalVariables.applicationContext.getBean("mobile", WebDriver.class);
            //}
        }

        GlobalVariables.currentScenario = scenario;
        List<String> listOfTags = new ArrayList<>(scenario.getSourceTagNames());
        GlobalVariables.currentTestcaseId = listOfTags.get(1).substring(1);
        try {
            File testdataFile = new File(
                    getClass().getClassLoader().getResource(GlobalVariables.currentTestcaseId + ".json").getFile());

            GlobalVariables.testDataJson = Utilities.getJson(testdataFile);
        } catch (Exception e) {
            Utilities.logExceptions(e);
            Assert.fail("Testdata File not present");
        }

        logger.log(Level.INFO,
                "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        logger.log(Level.INFO,
                "-------------------------------------------------------------------------------------#"
                        + GlobalVariables.currentTestcaseId + " - " + scenario.getName()
                        + "#-------------------------------------------------------------------------------------");
        logger.log(Level.INFO,
                "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

    }

    /**
     * Method to check if the user can logout from the application or to quit the
     * driver if unable to logout
     *
     * @param scenario
     * @throws IOException
     */
    @After
    public void afterScenario(Scenario scenario) throws IOException {
        if (scenario.isFailed() &&
                (scenario.getName().contains("Mobile") || scenario.getName().contains("Web"))) {

            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
            File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

            String timeStampForCapture = Utilities.generateTimestamp("yyyyMMddHHmmss");

            File destFile = new File("Screenshots/" + GlobalVariables.currentTestcaseId + "/TestcaseFailure-"
                    + timeStampForCapture + ".png");

            FileUtils.copyFile(srcFile, destFile);

            logger.log(Level.ERROR, "Testcase Id : " + GlobalVariables.currentTestcaseId + " Name : "
                    + GlobalVariables.currentScenario.getName() + " failed.");
        } else {
            logger.log(Level.INFO, "Testcase Id : " + GlobalVariables.currentTestcaseId + " Name : "
                    + GlobalVariables.currentScenario.getName() + " passed.");
        }

        if (!scenario.getName().contains("API")) {
            driver.quit();
        }

    }

    @AfterStep
    public void afterStepListener(Scenario scenario) {
        if (!scenario.isFailed()) {
            if (!scenario.getName().contains("API")) {
                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            }
        }
    }

}
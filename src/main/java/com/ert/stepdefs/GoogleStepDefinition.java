package com.ert.stepdefs;

import com.ert.config.variables.GlobalVariables;
import com.ert.libs.pages.EventPageForUser;
import com.ert.libs.pages.GoogleHomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class GoogleStepDefinition {
    private GoogleHomePage googleHomePage;
    private Logger logger = LogManager.getLogger("ApplicationLogs");
    private String sStatus;
    @Given("user launchs google application page")
    public void user_launchs_google_application_page() {
        System.out.println("this is google step 5");
        googleHomePage = GlobalVariables.applicationContext.getBean(GoogleHomePage.class);
        sStatus = googleHomePage.enterDataIntoGoogleFIeld();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.user_launchs_google_application_page(..) step failed : Propose a search button not displayed. "
                            + sStatus);
            Assert.fail("Propose a talk button not displayed. " + sStatus);
        }
        logger.log(Level.INFO, "StepDefinitions.verifyVisibilityOfProposeTalkButton(..) step passed");


    }

    @When("user enters in to filed")
    public void user_enters_in_to_filed() {
        googleHomePage.clickOnSearchButton();

    }

}

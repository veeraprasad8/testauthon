package com.ert.stepdefs;

import com.ert.config.variables.GlobalVariables;
import com.ert.libs.pages.AdminAreaPage;
import com.ert.libs.pages.EventPageForAdmin;
import com.ert.libs.pages.HomePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class AdminAreaStepDefinitions {
    private HomePage homePage;
    private AdminAreaPage adminAreaPage;
    private EventPageForAdmin eventPageForAdmin;
    private Logger logger = LogManager.getLogger("ApplicationLogs");
    private String sStatus;

    @Given("^The Events Creation Page is Displayed$")
    public void verifyHomePageIsDisplayed() {
        adminAreaPage = GlobalVariables.applicationContext.getBean(AdminAreaPage.class);

        sStatus = adminAreaPage.verifyPageIsDisplayed();

        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.verifyHomePageIsDisplayed(..) step failed : Event page is not displayed. "
                            + sStatus);
            Assert.fail("Event page is not displayed. " + sStatus);
        }
        logger.log(Level.INFO, "StepDefinitions.verifyHomePageIsDisplayed(..) step passed");
    }


    @When("^The User Creates a New Event$")
    public void createNewEvent() {
        adminAreaPage = GlobalVariables.applicationContext.getBean(AdminAreaPage.class);
        sStatus = adminAreaPage.clickNewEvnt();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.createNewEvent(..) step failed : New Event is not clicked. "
                            + sStatus);
            Assert.fail("New Event is not clicked. " + sStatus);
        }

        adminAreaPage = GlobalVariables.applicationContext.getBean(AdminAreaPage.class);
        sStatus = adminAreaPage.setEventDetails();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.createNewEvent(..) step failed : New Event is not Created. "
                            + sStatus);
            Assert.fail("New Event is not Created. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.createNewEvent(..) step passed");

    }

    @And("^The User opens the created event$")
    public void selectCreatedEvent() {
        adminAreaPage = GlobalVariables.applicationContext.getBean(AdminAreaPage.class);
        sStatus = adminAreaPage.openCreatedEvent();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.selectCreatedEvent(..) step failed : Created Event is not selected. "
                            + sStatus);
            Assert.fail("Created Event is not selected. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.selectCreatedEvent(..) step passed");
    }

    @When("The User opens the Talks list")
    public void openTalkList() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.openTalksList();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.openTalkList(..) step failed : Talk List not opened for event. "
                            + sStatus);
            Assert.fail("Talk List not opened for event. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.openTalkList(..) step passed");
    }

    @Then("The Proposed talk should be should be present")
    public void verifyProposedTalkIsPresent() {

        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.validatePresenceOfProposedTalk();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.validatePresenceOfProposedTalk(..) step failed : Proposed Talk not present. "
                            + sStatus);
            Assert.fail("Proposed Talk not present. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.validatePresenceOfProposedTalk(..) step passed");
    }

    @When("The User expands the talk")
    public void expandTheProposedTalk() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.expandTheProposedTalk();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.expandTheProposedTalk(..) step failed : Failed to Expand Proposed Talk. "
                            + sStatus);
            Assert.fail("Failed to Expand Proposed Talk. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.expandTheProposedTalk(..) step passed");
    }

    @Then("The User validates the information for the talk")
    public void validateProposedTalkDetails() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.validateProposedTalkDetails();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.validateProposedTalkDetails(..) step failed : Failed to Validate Proposed Talk details. "
                            + sStatus);
            Assert.fail("Failed to Validate Proposed Talk details. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.validateProposedTalkDetails(..) step passed");
    }

    @Then("The User is Navigated to the Created Event Page")
    public void validateCreatedEvent() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.verifyPageIsDisplayed();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.validateCreatedEvent(..) step failed : Failed to Validate Created Event Page. "
                            + sStatus);
            Assert.fail("Failed to Validate Created Event Page. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.validateCreatedEvent(..) step passed");
    }


    @And("The User validates the previously entered event details")
    public void validateEnteredDetailsInCreatedEvent() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.checkEnteredDetails();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.validateEnteredDetailsInCreatedEvent(..) step failed : Failed to Validate Entered Details In Created Event Page. "
                            + sStatus);
            Assert.fail("Failed to Validate Entered Details In Created Event Page. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.validateEnteredDetailsInCreatedEvent(..) step passed");
    }

    @When("^The User clicks the speaker registration toggle$")
    public void switchSpeakerRegToggle() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.toggleSpeakerRegistration();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.switchSpeakerRegToggle(..) step failed : Failed to Validate Switch Speaker Registration toggle. "
                            + sStatus);
            Assert.fail("Failed to Validate Switch Speaker Registration toggle. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.switchSpeakerRegToggle(..) step passed");
    }

    @And("^Adds \"([^\"]*)\" page$")
    public void addSpecificPage(String pageName) {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.addAPage(pageName);
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.addSpecificPage(..) step failed : Failed to Add specific page. "
                            + sStatus);
            Assert.fail("Failed to Add specific page. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.addSpecificPage(..) step passed");
    }

    @When("^The User adds the required pages$")
    public void addMultiplePages() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.addMultiplePages();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.addMultiplePages(..) step failed : Failed to Add multiple pages. "
                            + sStatus);
            Assert.fail("Failed to Add multiple pages. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.addMultiplePages(..) step passed");
    }

    @And("^Opens page settings$")
    public void openPageSettings() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.openPageSettings();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.openPageSettings(..) step failed : Failed to open page settings. "
                            + sStatus);
            Assert.fail("Failed to open page settings. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.openPageSettings(..) step passed");
    }

    @Then("^The User publishes the event$")
    public void publishEvent() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.publishEvent();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.publishEvent(..) step failed : Failed to publish event. "
                            + sStatus);
            Assert.fail("Failed to publish event. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.publishEvent(..) step passed");
    }

    @Then("The User performs validation on the presence of all pages and their sequence")
    public void validatePageSequence() {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.validateSequenceOfAddedPages();
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.validatePageSequence(..) step failed : Failed to validate sequence of added pages. "
                            + sStatus);
            Assert.fail("Failed to validate sequence of added pages. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.validatePageSequence(..) step passed");
    }

    @When("^The User modifies \"([^\"]*)\" page order to \"([^\"]*)\"$")
    public void modifyPageOrder(String pageName, String priority) {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.modifiyPageOrderPriority(pageName, priority);
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.modifyPageOrder(..) step failed : Failed to modify Page Order for " + pageName + " page. "
                            + sStatus);
            Assert.fail("Failed to modify Page Order for " + pageName + " page. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.modifyPageOrder(..) step passed");
    }

    @Then("^Validate if \"([^\"]*)\" page has become last in the list$")
    public void validatePageIsLast(String pageName) {
        eventPageForAdmin = GlobalVariables.applicationContext.getBean(EventPageForAdmin.class);
        sStatus = eventPageForAdmin.validatePageNameIsLast(pageName);
        if (!sStatus.equals("")) {
            logger.log(Level.ERROR,
                    "StepDefinitions.validatePageIsLast(..) step failed : Failed to validate if " + pageName + " page is at the end of the list. "
                            + sStatus);
            Assert.fail("Failed to validate if " + pageName + " page is at the end of the list. " + sStatus);
        }

        logger.log(Level.INFO, "StepDefinitions.validatePageIsLast(..) step passed");
    }
}

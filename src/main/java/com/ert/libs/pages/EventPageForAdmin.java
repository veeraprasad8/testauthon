package com.ert.libs.pages;

import com.ert.config.variables.GlobalVariables;
import com.ert.libs.commonUtils.Utilities;
import com.ert.libs.webActions.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class EventPageForAdmin {

    String xpathForPageTypesInDropdown = "//div[contains(text(), 'Page Type')]/..//span[text()='pageType']/parent::a";
    String xpathForProposedTalkPage = "//tbody/tr[1]//span[text()='talkTitle']";
    String xpathForEventTitle = "//h1/span[text()='title']";
    String xpathForProposedTalkExpand = "//tbody/tr[1]//span[text()='talkTitle']/../../../../div[1]/span";
    String xpathForPageCreated = "//h2[contains(text(),'pageType')]";
    String xpathForPageTitlesInSettingsPage = "//div[@id='root']//tbody//tr[not(@class)]//td/div/div/div[1]/span";
    String xpathForPageEditButton = "//div/span[text()='pageN']/../following-sibling::div[4]/button[1]";
    String xpathForMouseOver = "//div/span[text()='pageN']/../following-sibling::div[4]";
    String xpathForPriorityOfPage = "//div/span[text()='Agenda']/../following-sibling::div[2][text()='order']";

    private boolean bStatus;

    @FindBy(xpath = "//div[text()='Order*']/..//input")
    private WebElement orderTextBoxInEditMenu;
    @FindBy(xpath = "//div[@class='popup-footer']/button[text()='Save']")
    private WebElement saveButtonInEditMenu;
    @FindBy(xpath = "//span[text()='Pages']/../..")
    private WebElement checkPagesTabOpen;
    @FindBy(xpath = "//span[text()='Pages']/..")
    private WebElement pagesTab;
    @FindBy(xpath = "//button[@id='add-page']")
    private WebElement addPage;
    @FindBy(xpath = "//div[contains(text(), 'Page Type')]/following-sibling::div//button")
    private WebElement selectPageType;
    @FindBy(xpath = "//span[text()='Page settings']/..")
    private WebElement pageSettings;
    @FindBy(xpath = "//span[text()='Event Agenda']/..")
    private WebElement eventAgenda;
    @FindBy(xpath = "//span[text()='Talks list']/..")
    private WebElement talksList;
    @FindBy(xpath = "//input[@placeholder='Search']")
    private WebElement searchBox;
    @FindBy(xpath = "//td/p[text()='Talk Description']/following-sibling::div//p")
    private WebElement talkDescriptionEle;
    @FindBy(xpath = "//td/p[text()='Attachments']/following-sibling::p/a")
    private WebElement attachmentEle;
    @FindBy(xpath = "//div[contains(@style,'display: block')]//button[text()='Publish Event']")
    private WebElement publishEvent;
    @FindBy(xpath = "//button[text()='Publish']")
    private WebElement publishButtonOnPopup;
    @FindBy(xpath = "//span[text()='Speaker reg']/following-sibling::label")
    private WebElement speakerRegToggle;
    @FindBy(xpath = "//button[text()='Ok']")
    private WebElement okButton;
    @FindBy(xpath = "//button[text()='Add']")
    private WebElement addButton;
    @FindBy(xpath = "//p[text()='Event Title*']/..//input")
    private WebElement actEventTitle;
    @FindBy(xpath = "//p[text()='Suggested URL*']/..//input")
    private WebElement actSuggestedUrl;
    @FindBy(xpath = "//p[text()='Event Days*']/..//input[@id='start_date']")
    private WebElement actStartDate;
    @FindBy(xpath = "//p[text()='Event Days*']/..//input[@id='end_date']")
    private WebElement actEndDate;
    @FindBy(xpath = "//p[text()='Event Privacy Settings']/../div/button/span[1]")
    private WebElement actEventPrivacySetting;
    @FindBy(xpath = "//input[@id='unlisted'][not(@checked)]/following-sibling::label")
    private WebElement actEventPrivacyListingCheckbox;

    private WebDriver driver;
    @Autowired
    private Environment environment;
    @Autowired
    private ElementActions elementActions;

    public EventPageForAdmin(@Autowired @Qualifier("chrome") WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public String openTalksList() {

        try {

            bStatus = elementActions.waitAndClickUsingWebElement(eventAgenda, 30);
            if (!bStatus) {
                return "Failed to wait and click on Event Agenda";
            }

            bStatus = elementActions.waitAndClickUsingWebElement(talksList, 10);
            if (!bStatus) {
                return "Failed to wait and click on Talks List";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";

    }

    public String validatePresenceOfProposedTalk() {

        try {

            Map<String, String> talkDetails = Utilities.getMapFromJson("TalkDetails");

            String talkTitle = talkDetails.get("talkTitle");

            bStatus = elementActions.waitAndEnterTextUsingWebElement(searchBox, talkTitle, 30);
            if (!bStatus) {
                return "Failed to wait and enter Proposed talk title in search box";
            }

            By locatorForProposedTalk = By.xpath(xpathForProposedTalkPage.replace("talkTitle", talkTitle));

            bStatus = elementActions.waitForVisibilityOfElementLocatedBy(locatorForProposedTalk, 10);
            if (!bStatus) {
                return "Proposed Talk not listed in talk list";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";

    }

    public String expandTheProposedTalk() {

        try {

            Map<String, String> talkDetails = Utilities.getMapFromJson("TalkDetails");

            String talkTitle = talkDetails.get("talkTitle");

            By locatorForProposedTalkExpandButton = By.xpath(xpathForProposedTalkExpand.replace("talkTitle", talkTitle));

            bStatus = elementActions.waitAndClickUsingBy(locatorForProposedTalkExpandButton, 10);
            if (!bStatus) {
                return "Proposed Talk not expanded";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

    public String validateProposedTalkDetails() {
        try {

            Map<String, String> talkDetails = Utilities.getMapFromJson("TalkDetails");

            String talkDesc = talkDetails.get("about");
            String attachment = talkDetails.get("attachment");

            String expDesc = elementActions.getText(talkDescriptionEle);
            if (!expDesc.equals(talkDesc)) {
                return "Actual Description, " + expDesc + ", does not match Expected Description, " + talkDesc;
            }

            String expAttachment = elementActions.getText(attachmentEle);

            if (!expAttachment.equals(attachment)) {
                return "Actual Attachment, " + expAttachment + ", does not match Expected Attachment, " + attachment;
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

    public String addMultiplePages() {

        try {

            List<String> pagesToBeAdded = Utilities.getListOfStringFromJson("EventDetails.pages");

            GlobalVariables.pagesAdded = pagesToBeAdded;

            for (String page : pagesToBeAdded) {
                String status = addAPage(page);
                if (!status.equals("")) {
                    return "Failed to add page, " + page + ". " + status;
                }
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";

    }

    public String addAPage(String pageName) {

        try {

            bStatus = elementActions.waitForInvisibilityOfElementBy(By.xpath("//div[@class='popup-bg'][contains(@style,'display: block')]"), 10);
            if (!bStatus) {
                return "Failed to wait for publish event";
            }

            if (!checkPagesTabOpen.getAttribute("class").contains("open")) {
                bStatus = elementActions.clickWebElementUsingJse(pagesTab);
                if (!bStatus) {
                    return "Failed to click on the pages tab";
                }
            }

            bStatus = elementActions.waitAndClickUsingWebElement(addPage, 10);
            if (!bStatus) {
                return "Failed to wait and click on the add page button";
            }

            bStatus = elementActions.waitAndClickUsingWebElement(selectPageType, 10);
            if (!bStatus) {
                return "Failed to wait and click on the select page type dropdown";
            }

            By loactorForPageType = By.xpath(xpathForPageTypesInDropdown.replace("pageType", pageName));

            bStatus = elementActions.waitAndClickUsingBy(loactorForPageType, 10);
            if (!bStatus) {
                return "Failed to wait and select the page type, " + pageName;
            }

            bStatus = elementActions.clickUsingWebElement(addButton);
            if (!bStatus) {
                return "Failed to click on add button";
            }

            By locatorForCreatedPageHeading = By.xpath(xpathForPageCreated.replace("pageType", pageName));

            bStatus = elementActions.waitForVisibilityOfElementLocatedBy(locatorForCreatedPageHeading, 10);
            if (!bStatus) {
                return "Failed to navigate to created page template";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

    public String openPageSettings() {

        try {

            bStatus = elementActions.waitForInvisibilityOfElementBy(By.xpath("//div[@class='popup-bg'][contains(@style,'display: block')]"), 10);
            if (!bStatus) {
                return "Failed to wait for publish event";
            }

            bStatus = elementActions.waitAndClickUsingWebElement(pageSettings, 10);
            if (!bStatus) {
                return "Failed to wait and click on page settings tab";
            }

            bStatus = elementActions.waitForVisibilityOfElementLocatedBy(By.xpath(xpathForPageTitlesInSettingsPage), 10);
            if (!bStatus) {
                return "Failed to navigate to Page Settings page.";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

    public String validateSequenceOfAddedPages() {
        try {

            List<String> pagesToBeAdded = GlobalVariables.pagesAdded;

            List<WebElement> pagesThatAreAdded = driver.findElements(By.xpath(xpathForPageTitlesInSettingsPage));

            for (int ctr = 0; ctr < pagesThatAreAdded.size(); ctr++) {

                String expPageName = pagesToBeAdded.get(ctr);
                String actPageName = pagesThatAreAdded.get(ctr).getText();

                if (!expPageName.equals(actPageName)) {
                    return "Expected page, " + expPageName + " , but found " + actPageName;
                }
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    public String toggleSpeakerRegistration() {
        try {

            bStatus = elementActions.waitAndClickUsingWebElement(speakerRegToggle, 20);
            if (!bStatus) {
                return "Failed to wait and click on speaker registration toggle";
            }

            bStatus = elementActions.waitForVisibilityOfElement(okButton, 10);
            if (!bStatus) {
                return "Failed to wait for ok button on the popup";
            }

            bStatus = elementActions.clickWebElementUsingJse(okButton);
            if (!bStatus) {
                return "Failed to click on ok button on the popup";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

    public String publishEvent() {
        try {

            bStatus = elementActions.waitForInvisibilityOfElementBy(By.xpath("//div[@class='popup-bg'][contains(@style,'display: block')]"), 10);
            if (!bStatus) {
                return "Failed to wait for publish event";
            }

            bStatus = elementActions.waitAndClickUsingWebElement(publishEvent, 10);
            if (!bStatus) {
                return "Failed to click on publish event";
            }

            bStatus = elementActions.waitForVisibilityOfElement(publishButtonOnPopup, 10);
            if (!bStatus) {
                return "Failed to wait for publish button on the popup";
            }

            bStatus = elementActions.clickWebElementUsingJse(publishButtonOnPopup);
            if (!bStatus) {
                return "Failed to click on publish button on the popup";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

    public String verifyPageIsDisplayed() {
        try {
            String eventTitl = xpathForEventTitle.replace("title", GlobalVariables.eventTitle);
            bStatus = elementActions.waitForVisibilityOfElementLocatedBy(By.xpath(eventTitl), GlobalVariables.shortWait);
            if (!bStatus) {
                return "Failed to navigate to Created Event Page";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    private String getEventTitle() {
        return actEventTitle.getAttribute("value");
    }

    private String getSuggestedUrll() {
        return actSuggestedUrl.getAttribute("value");
    }

    private String getStartDate() {
        return actStartDate.getAttribute("value");
    }

    private String getEndDate() {
        return actEndDate.getAttribute("value");
    }

    private String getEventPrivacy() {
        return actEventPrivacySetting.getText();
    }

    public String checkEnteredDetails() {

        try {
            Map<String, String> eventDetails = Utilities.getMapFromJson("EventDetails");
            String actEventTitle = getEventTitle();
            if (!actEventTitle.equals(GlobalVariables.eventTitle)) {
                return "Mismatch in event title, Actual, " + actEventTitle + " but Expected, " + GlobalVariables.eventTitle;
            }

            String actSuggestedUrl = getSuggestedUrll();

            if (!actSuggestedUrl.equalsIgnoreCase(GlobalVariables.suggestedUrl)) {
                return "Mismatch in Suggested URL, Actual, " + actSuggestedUrl + " but Expected, " + GlobalVariables.suggestedUrl;
            }


            String actStartDate = getStartDate();

            if (!actStartDate.equals(eventDetails.get("startDate"))) {
                return "Mismatch in Start Date, Actual, " + actStartDate + " but Expected, " + eventDetails.get("startDate");
            }

            String actEndDate = getEndDate();

            if (!actEndDate.equals(eventDetails.get("endDate"))) {
                return "Mismatch in End Date, Actual, " + actEndDate + " but Expected, " + eventDetails.get("endDate");
            }

            String actEventPrivacy = getEventPrivacy();

            if (!actEventPrivacy.equals("Public")) {
                return "Mismatch in Event Privacy Setting, Actual , " + actEventPrivacy + " but Expected, " + "Public";
            }

            elementActions.scrollToViewWithWebElement(actEventPrivacyListingCheckbox);
            bStatus = elementActions.waitForVisibilityOfElement(actEventPrivacyListingCheckbox, GlobalVariables.shortWait);
            if (!bStatus) {
                return "Event Privacy Listing CheckBox is Checked";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }


    public String modifiyPageOrderPriority(String pageName, String orderNumber) {

        try {

            String xpthPageName = xpathForMouseOver.replace("pageN", pageName);
            String xpthPageEditButton = xpathForPageEditButton.replace("pageN", pageName);

            bStatus = elementActions.mouseHover(By.xpath(xpthPageName));
            if (!bStatus) {
                return "Failed to hover to action container of page";
            }
            bStatus = elementActions.clickUsingBy(By.xpath(xpthPageEditButton));
            if (!bStatus) {
                return "Failed to click on edit button";
            }
            bStatus = elementActions.clearAndEnterTextUsingWebElement(orderTextBoxInEditMenu, orderNumber);
            if (!bStatus) {
                return "Failed to set the priority in popup";
            }
            bStatus = elementActions.clickUsingWebElement(saveButtonInEditMenu);
            if (!bStatus) {
                return "Failed to click on save button in popup";
            }

            String xpthForPagePriority = xpathForPriorityOfPage.replace("pageN", pageName).replace("order", orderNumber);

            bStatus = elementActions.waitForVisibilityOfElementLocatedBy(By.xpath(xpthForPagePriority), 10);
            if (!bStatus) {
                return "Failed to wait for updated page priority";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";

    }

    public String validatePageNameIsLast(String pageName) {

        try {

            List<WebElement> pagesThatAreAdded = driver.findElements(By.xpath(xpathForPageTitlesInSettingsPage));

            String actPageName = pagesThatAreAdded.get(pagesThatAreAdded.size() - 1).getText();

            if (!actPageName.equals(pageName)) {
                return "Expected page, " + pageName + ", did not match Actual page, " + actPageName + ", at the end of the list";
            }

        } catch (Exception e) {

            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

}
package com.ert.libs.pages;

import com.ert.config.variables.GlobalVariables;
import com.ert.libs.commonUtils.Utilities;
import com.ert.libs.webActions.ElementActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class GoogleHomePage {


    String xpathForEventCard = "//span[text()='eventTitle']/../../../../..";
    @FindBy(name = "q")
    private WebElement googleSearchInput;

    @FindBy(name = "btnK")
    private WebElement googleSearchBtn;

    private boolean bStatus;
    private WebDriver driver;
    @Autowired
    private Environment environment;
    @Autowired
    private ElementActions elementActions;

    public GoogleHomePage(@Autowired @Qualifier("chrome") WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public String enterDataIntoGoogleFIeld()  {
        try {
            Map<String, String> talkDetails = Utilities.getMapFromJson("GoogleDetails");
            bStatus = elementActions.waitAndEnterTextUsingWebElement(googleSearchInput, talkDetails.get("setText"), GlobalVariables.shortWait);
            if (!bStatus) {
                return "Failed to enter Template in SearchBox ";
            }
        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

    public void clickOnSearchButton(){
        elementActions.clickUsingWebElement(googleSearchBtn);
    }

}

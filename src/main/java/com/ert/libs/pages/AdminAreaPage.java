package com.ert.libs.pages;

import com.ert.config.variables.GlobalVariables;
import com.ert.libs.commonUtils.Utilities;
import com.ert.libs.webActions.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@Component
public class AdminAreaPage {

    @FindBy(xpath = "//a[text()='New event']")
    private WebElement newEvent;

    @FindBy(xpath = "//div[text()='Event Title*']/..//input")
    private WebElement eventTitle;

    @FindBy(xpath = "//input[@name='start']")
    private WebElement startDate;

    @FindBy(xpath = "//div[@class='datepicker-days']/table/thead//th[@class='datepicker-switch']")
    private WebElement monthYearSelection;

    @FindBy(xpath = "//div[@class='datepicker-days']/table/thead//th[@class='next']")
    private WebElement nextMonthButton;


    @FindBy(xpath = "//input[@name='end']")
    private WebElement endDate;

    @FindBy(xpath = "//div[text()='Event Privacy Settings']/..//button")
    private WebElement eventPrivacySetting;

    @FindBy(xpath = "//div[text()='Event Privacy Settings']/..//div/ul/li[2]/a")
    private WebElement publicSetting;

    @FindBy(xpath = " //button[text()='Next']")
    private WebElement nextButton;

    @FindBy(xpath = "//section/div[@class='info-panel-body']//input[@placeholder='Search']")
    private WebElement searchBoxInPopup;

    @FindBy(xpath = "//p[text()='Empty Template']/../span")
    private WebElement emptyTemplate;

    @FindBy(xpath = "//button[text()='Create Event']")
    private WebElement createEvent;

    @FindBy(xpath = "//input[@placeholder='Search']")
    private WebElement searchBox;

    @FindBy(xpath = "//span[@class ='disabled-url ']")
    private WebElement suggestedUrl;


    private String xpathForCreatedEvent = "//a[text()='eventTitle']";

    private String xpathForDay = "//td[(@class='day' or @class = 'today day' ) and text()='dd']";


    private boolean bStatus;
    private WebDriver driver;

    @Autowired
    private Environment environment;
    @Autowired
    private ElementActions elementActions;

    public AdminAreaPage(@Autowired @Qualifier("chrome") WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    /**
     * Method to verify if the home page is displayed
     *
     * @return
     */
    public String verifyPageIsDisplayed() {
        try {
            bStatus = elementActions.waitForVisibilityOfElement(newEvent, GlobalVariables.shortWait);
            if (!bStatus) {
                return "Failed to wait for visibility of the new Event button";
            }
        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    /**
     * Method to click on New Event
     *
     * @return
     */
    public String clickNewEvnt() {

        try {
            bStatus = elementActions.clickUsingWebElement(newEvent);
            if (!bStatus) {
                return "Failed to New Event button";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    /**
     * Method to set event details
     *
     * @return
     * @throws IOException
     */
    public String setEventDetails() {
        try {
            Map<String, String> eventDetails = Utilities.getMapFromJson("EventDetails");
            String startDat = "";
            String endDat = "";

            GlobalVariables.eventTitle = eventDetails.get("setTitle") + Utilities.generateRandomAlphanumeric(6);
            startDat = eventDetails.get("startDate");

            bStatus = elementActions.waitAndEnterTextUsingWebElement(eventTitle, GlobalVariables.eventTitle, GlobalVariables.shortWait);
            if (!bStatus) {
                return "Failed to Set Title " + GlobalVariables.eventTitle;

            }

            getSuggestedUrl();

            bStatus = elementActions.clickUsingWebElement(startDate);
            if (!bStatus) {
                return "Failed to Click Date Picker";

            }

            GlobalVariables.date = startDat;
            startDate(startDat);

            bStatus = elementActions.clickWebElementUsingJse(eventPrivacySetting);
            if (!bStatus) {
                return "Failed to Click Event Privacy Setting ";

            }

            bStatus = elementActions.clickWebElementUsingJse(publicSetting);
            if (!bStatus) {
                return "Failed to Select Public Setting ";

            }

            bStatus = elementActions.clickWebElementUsingJse(nextButton);
            if (!bStatus) {
                return "Failed to Select Public Setting ";

            }

            bStatus = elementActions.waitAndEnterTextUsingWebElement(searchBoxInPopup, "empty template", GlobalVariables.shortWait);
            if (!bStatus) {
                return "Failed to enter Template in SearchBox ";

            }

            bStatus = elementActions.waitForVisibilityOfElement(emptyTemplate, GlobalVariables.shortWait);
            if (!bStatus) {
                return "Select Template page not displayed";
            }

            bStatus = elementActions.clickUsingWebElement(emptyTemplate);
            if (!bStatus) {
                return "Empty template not selected";
            }

            bStatus = elementActions.clickUsingWebElement(createEvent);
            if (!bStatus) {
                return "Create event button not clicked";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    public String startDate(String startDat) throws Exception {

        String months[] = {"January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"};

        DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        Date d = sdf.parse(startDat);

        Calendar cal = Calendar.getInstance();

        cal.setTime(d);

        int day = cal.get(Calendar.DAY_OF_MONTH);

        int month = cal.get(Calendar.MONTH);

        int year = cal.get(Calendar.YEAR);

        String strtDate = months[month] + " " + year;
        selectMonth(strtDate);
        selectDay(day);

        return "";

    }


    private void getSuggestedUrl() {
        String url = suggestedUrl.getText();
        GlobalVariables.suggestedUrl = url + GlobalVariables.eventTitle;
    }

    private void selectDay(int dayToBeSelected) {
        String day = xpathForDay.replace("dd", String.valueOf(dayToBeSelected));
        driver.findElement(By.xpath(day)).click();

    }

    private void selectMonth(String monthToBeSelected) {

        while (!monthYearSelection.getText().equalsIgnoreCase(monthToBeSelected))
            nextMonthButton.click();

    }

    public String openCreatedEvent() {
        try {

            bStatus = elementActions.waitAndEnterTextUsingWebElement(searchBox, GlobalVariables.eventTitle, 30);
            if (!bStatus) {
                return "Failed to wait and enter event title in search box";
            }

            By locatorForeventEle = By.xpath(xpathForCreatedEvent.replace("eventTitle", GlobalVariables.eventTitle));

            bStatus = elementActions.waitAndClickUsingBy(locatorForeventEle, 30);
            if (!bStatus) {
                return "Failed to wait and click on event in search results";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";

    }
}

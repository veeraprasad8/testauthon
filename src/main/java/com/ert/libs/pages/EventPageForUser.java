package com.ert.libs.pages;

import com.ert.config.variables.GlobalVariables;
import com.ert.libs.commonUtils.Utilities;
import com.ert.libs.webActions.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class EventPageForUser {

    String xpathForPageTabInEventPage = "//div[@class='evnt-event-header']//a[text()='pageName']";
    String xpathForPageTitle = "//h1[text()='pageName']";
    String xpathForAllPageTabs = "//div[@class='evnt-event-header']//a/parent::li/a";

    @FindBy(xpath = "//button[text()='Propose a talk']")
    WebElement proposeTalk;
    @FindBy(xpath = "//label[text()='Talk Title']//following-sibling::input")
    WebElement inputForTalkTitle;
    @FindBy(xpath = "//body[@id='tinymce']")
    WebElement inputForAbout;
    @FindBy(xpath = "//label[text()='Attached materials']/../div/div[1]//input")
    WebElement attachment;
    @FindBy(xpath = "//button[text()='Register']")
    WebElement registerForm;
    @FindBy(xpath = "//p[text()='Thanks for your registration!']")
    WebElement regPage;
    @FindBy(xpath = "//div[@class='evnt-text']/h1")
    WebElement successMessage;
    @FindBy(xpath = "//div[contains(@class,'evnt-content-text')]//h1")
    private WebElement eventTitleOnPage;
    @FindBy(xpath = "//span[@class='date']")
    private WebElement datesOnPage;
    @FindBy(xpath = "//h1/following-sibling::p")
    private WebElement speakerPageDescription;

    private boolean bStatus;
    private WebDriver driver;
    @Autowired
    private Environment environment;
    @Autowired
    private ElementActions elementActions;

    public EventPageForUser(@Autowired @Qualifier("chrome") WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public String verifyProposeATalkButtonIsDisplayed() {
        try {

            bStatus = elementActions.waitForVisibilityOfElement(proposeTalk, GlobalVariables.shortWait);
            if (!bStatus) {
                return "Failed to wait for visibility of propose a talk button";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    public String clickOnProposeATalkButton() {
        try {

            bStatus = elementActions.clickUsingWebElement(proposeTalk);
            if (!bStatus) {
                return "Failed to click on the propose a talk button";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    public String fillsTheFormForTheTalk() {
        try {

            bStatus = elementActions.waitForVisibilityOfElement(inputForTalkTitle, GlobalVariables.shortSync);
            if (!bStatus) {
                return "Faialed to wait for visibility of talk title input box";
            }

            Map<String, String> talkDetails = Utilities.getMapFromJson("TalkDetails");

            bStatus = elementActions.enterTextUsingWebElement(inputForTalkTitle, talkDetails.get("talkTitle"));
            if (!bStatus) {
                return "Failed to enter text for Talk Title";
            }

            elementActions.switchToFrameByName("talks_description_ifr");

            bStatus = elementActions.enterTextUsingWebElement(inputForAbout, talkDetails.get("about"));
            if (!bStatus) {
                return "Failed to enter text for Talk Title";
            }

            elementActions.switchToDefaultContent();

            bStatus = elementActions.enterTextUsingWebElement(attachment, System.getProperty("user.dir") + "\\src\\test\\resources\\" + talkDetails.get("attachment"));
            if (!bStatus) {
                return "Failed to enter text for Talk Title";
            }

            bStatus = elementActions.clickUsingWebElement(registerForm);
            if (!bStatus) {
                return "Unable to click on register button";
            }


        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

    public String verifySuccessMessage() {
        try {

            elementActions.waitForVisibilityOfElement(regPage, GlobalVariables.shortWait);

            String actualMessage = elementActions.getText(successMessage);

            String expMessage = Utilities.getStringValueFromJson("SuccessMessage");

            if (!actualMessage.equalsIgnoreCase(expMessage)) {
                return "Expected Message was " + expMessage + " but Actual Message is " + actualMessage;
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";
    }

    public String clickOnSpecificPage(String pageName) {

        try {

            By locatorForPageTab = By.xpath(xpathForPageTabInEventPage.replace("pageName", pageName));

            bStatus = elementActions.waitAndClickUsingBy(locatorForPageTab, 10);
            if (!bStatus) {
                return "Failed to wait and click on the " + pageName + " page tab";
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";

    }

    public String validateDetailsForPage(String pageName) {

        try {

            Map<String, String> pageDataToBeValidated = Utilities.getMapFromJson("PageValidations.['" + pageName + "']");

            for (Map.Entry<String, String> dataParam : pageDataToBeValidated.entrySet()) {
                String status = invokeSpecificKeyword(dataParam.getKey(), dataParam.getValue());
                if (!status.equals("")) {
                    return "Failed to validate the field, " + dataParam + ". " + status;
                }
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";

    }

    private String invokeSpecificKeyword(String keyword, String data) {
        try {

            String status = "";

            switch (keyword) {
                case "Title":
                    status = validateEventTitleForPage(GlobalVariables.eventTitle);
                    if (!status.equals("")) {
                        return "Failed to validate title for page. " + status;
                    }
                    break;
                case "Dates":
                    status = validateDatesForPage(GlobalVariables.date);
                    if (!status.equals("")) {
                        return "Failed to validate dates for page. " + status;
                    }
                    break;
                case "PageTitle":
                    status = validatePageTitleForPage(data);
                    if (!status.equals("")) {
                        return "Failed to validate page title for page. " + status;
                    }
                    break;
                case "PageDescription":
                    status = validatePageDescriptionForPage(data);
                    if (!status.equals("")) {
                        return "Failed to validate page description for page. " + status;
                    }
                    break;
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    private String validatePageDescriptionForPage(String pageDesc) {
        try {

            String actPageDescription = speakerPageDescription.getText();
            if (!actPageDescription.equals(pageDesc)) {
                return "Expected page desc, " + pageDesc + ", does not match Actual page desc, " + actPageDescription;
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    private String validatePageTitleForPage(String pageTitle) {
        try {

            By locatorForPageTitle = By.xpath(xpathForPageTitle.replace("pageName", pageTitle));

            bStatus = elementActions.waitForVisibilityOfElementLocatedBy(locatorForPageTitle, 10);
            if (!bStatus) {
                return "Page Title is not the same as expected title, " + pageTitle;
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    private String validateDatesForPage(String eventDate) {
        try {

            String actDate = datesOnPage.getText();

            DateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy");
            DateFormat convertedFormat = new SimpleDateFormat("dd MMM yyyy");
            Date date = originalFormat.parse(eventDate);

            eventDate = convertedFormat.format(date);

            if (!actDate.equals(eventDate)) {
                return "Expected date, " + eventDate + ", does not match Actual date , " + actDate;
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    private String validateEventTitleForPage(String eventTitle) {
        try {

            String actTitle = eventTitleOnPage.getText();

            if (!actTitle.equals(eventTitle)) {
                return "Expected title, " + eventTitle + ", does not match Actual title , " + actTitle;
            }

        } catch (Exception e) {
            Utilities.logExceptions(e);
            return "Exception Thrown";
        }
        return "";
    }

    public String validateSequenceOfPageTabs() {

        try {

            List<WebElement> listOfPageTabs = driver.findElements(By.xpath(xpathForAllPageTabs));

            List<String> expListOfPageTabs = new ArrayList<String>(Utilities.getMapOfMapFromJson("PageValidations").keySet());

            for (int ctr = 0; ctr < listOfPageTabs.size(); ctr++) {

                String expPageName = expListOfPageTabs.get(ctr);
                String actPageName = listOfPageTabs.get(ctr).getText();

                if (!expPageName.equalsIgnoreCase(actPageName)) {
                    return "Expected page, " + expPageName + " , but found " + actPageName;
                }
            }


        } catch (Exception e) {

            Utilities.logExceptions(e);
            return "Exception Thrown";
        }

        return "";

    }


}
